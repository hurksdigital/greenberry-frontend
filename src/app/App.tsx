import React from 'react'
import FormSelect from '../components/form/select/FormSelect'
import './app.scss'
import GraphQL from '../backend/GraphQL'
import Person from 'greenberry-common/models/entity/Person'
import Species from 'greenberry-common/models/entity/Species'
import Film from 'greenberry-common/models/entity/Film'
import HttpResponse from '../backend/HttpResponse'
import { StarwarsCharacter } from '../components/starwars-character/StarwarsCharacter'
import background from './background.jpg'

/**
 * The state
 */
interface AppState {
    /**
     * The data for all selects
     */
    select: {
        /**
         * The values
         */
        values: {
            /**
             * The selected person
             */
            people: string | null

            /**
             * The selected species
             */
            species: string | null

            /**
             * The selected film
             */
            films: string | null
        }

        /**
         * The options
         */
        options: {
            /**
             * All people options
             */
            people: Partial<Person>[]

            /**
             * All species options
             */
            species: Partial<Species>[]

            /**
             * All film options
             */
            films: Partial<Film>[]
        }
    }

    /**
     * All star wars people to display
     */
    people: Person[]
}

/**
 * The main app entry point.
 * 
 * @author Stan Hurks
 */
export default class App extends React.Component<any, AppState> {

    constructor(props: any) {
        super(props)

        this.state = {
            select: {
                values: {
                    people: null,
                    species: null,
                    films: null
                },
                options: {
                    people: [],
                    species: [],
                    films: []
                }
            },
            people: []
        }
    }

    public componentDidMount = () => {
        this.updateData()
    }

    public render = () => {
        return (
            <div id="app" style={{
                backgroundImage: `url(${background})`
            }}>
                <div id="app-form">
                    <div id="app-form-selects">
                        <div className="app-form-selects-select">
                            <FormSelect
                                placeholder="Select a film"
                                options={this.state.select.options.films.map((film) => ({
                                    key: film.uuid as string,
                                    value: film.title as string
                                }))}
                                onChange={(films) => {
                                    this.setState({
                                        select: {
                                            ...this.state.select,
                                            values: {
                                                ...this.state.select.values,
                                                films
                                            }
                                        }
                                    }, () => {
                                        this.updatePeople()
                                    })
                                }}
                                value={this.state.select.values.films} />
                        </div><div className="app-form-selects-select">
                            <FormSelect
                                placeholder="Select a species"
                                options={this.state.select.options.species.map((species) => ({
                                    key: species.uuid as string,
                                    value: species.name as string
                                }))}
                                onChange={(species) => {
                                    this.setState({
                                        select: {
                                            ...this.state.select,
                                            values: {
                                                ...this.state.select.values,
                                                species
                                            }
                                        }
                                    }, () => {
                                        this.updatePeople()
                                    })
                                }}
                                value={this.state.select.values.species} />
                        </div><div className="app-form-selects-select">
                            <FormSelect
                                placeholder="Select character"
                                options={this.state.select.options.people.map((person) => ({
                                    key: person.uuid as string,
                                    value: person.name as string
                                }))}
                                onChange={(people) => {
                                    this.setState({
                                        select: {
                                            ...this.state.select,
                                            values: {
                                                ...this.state.select.values,
                                                people
                                            }
                                        }
                                    }, () => {
                                        this.updatePeople()
                                    })
                                }}
                                value={this.state.select.values.people} />
                        </div>
                    </div><div id="app-form-entries">
                        {
                            this.state.people.map((person) =>
                                <StarwarsCharacter character={person} key={person.uuid} />
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Updates all data
     */
    private updateData = () => {
        GraphQL.request({
            query: `
                query {
                    getPeople(input:{
                        filmUuid: ${this.state.select.values.films === null ? 'null' : '"' + this.state.select.values.films + '"'},
                        speciesUuid: ${this.state.select.values.species === null ? 'null' : '"' + this.state.select.values.species + '"'},
                        characterUuid: ${this.state.select.values.people === null ? 'null' : '"' + this.state.select.values.people + '"'}
                    }){
                        uuid
                        edited
                        name
                        films{
                            uuid
                            title
                        }
                        species{
                            uuid
                            name
                        }
                    }
                    getCharacters{
                        uuid
                        name
                    }
                    getSpecies {
                        uuid
                        name
                    }
                    getFilms {
                        uuid
                        title
                    }
                }
            `
        }).then((res: HttpResponse<{
            getPeople: Person[]

            getCharacters: Partial<Person>[]

            getSpecies: Partial<Species>[]

            getFilms: Partial<Film>[]
        }>) => {
            this.setState({
                select: {
                    ...this.state.select,
                    options: {
                        people: res.data.getCharacters,
                        species: res.data.getSpecies,
                        films: res.data.getFilms
                    }
                },
                people: res.data.getPeople
            })
        }).catch((ex) => {
            console.error('Could not update data: ', ex)
        })
    }

    /**
     * Updates the list of people
     */
    private updatePeople = () => {
        GraphQL.request({
            query: `
                query {
                    getPeople(input:{
                        filmUuid: ${this.state.select.values.films === null ? 'null' : '"' + this.state.select.values.films + '"'},
                        speciesUuid: ${this.state.select.values.species === null ? 'null' : '"' + this.state.select.values.species + '"'},
                        characterUuid: ${this.state.select.values.people === null ? 'null' : '"' + this.state.select.values.people + '"'}
                    }){
                        uuid
                        edited
                        name
                        films{
                            uuid
                            title
                        }
                        species{
                            uuid
                            name
                        }
                    }
                }
            `
        }).then((res: HttpResponse<{
            getPeople: Person[]
        }>) => {
            this.setState({
                people: res.data.getPeople
            })
        }).catch((ex) => {
            console.error('Could not update data: ', ex)
        })
    }
}