import { Subject } from 'rxjs'
import HttpResponse from './HttpResponse'

/**
 * All core functionality to connect the GraphQL server with the front-end.
 * 
 * Also contains subjects for when the API is not available / offline / 500 / 401.
 * 
 * @author Stan Hurks
 */
export default abstract class GraphQL {

    /**
     * Whenever the API is not reachable
     */
    public static readonly apiUnreachable: Subject<void> = new Subject()

    /**
     * Whenever the user is unauthorized
     */
    public static readonly unauthorized: Subject<void> = new Subject()

    /**
     * Whenever the server has an error
     */
    public static readonly internalServerError: Subject<void> = new Subject()

    /**
     * Whenever the progress percentage changes in the requests it will be fired here.
     */
    public static readonly requestProgressPercentage: Subject<number> = new Subject()

    /**
     * The percentages of which every pending XMLHttpRequest is done
     */
    private static readonly requestPercentages: { [index: number]: number } = {}

    /**
     * The amount of requests done
     */
    private static requestCounter: number = 0

    /**
     * The HTTP options
     */
    private static options = {
        base: 'http://localhost:4000'
    }

    /**
     * Makes a XMLHttpRequest of a GraphQL query
     * @param payload the payload (GraphQL query syntax)
     * @param headers optional: additional request headers
     */
    public static request = (
        payload: {
            /**
             * The query to perform in GraphQL
             */
            query?: string

            /**
             * The mutation to perform in GraphQL
             */
            mutation?: string
        },
        headers?: { [key: string]: string }
    ): Promise<HttpResponse<any>> => {

        return new Promise((resolve, reject) => {
            // Initialize the request
            const request = new XMLHttpRequest()
            request.open('POST', GraphQL.options.base)
            request.responseType = 'json'

            // Send the request headers GraphQL asks for
            request.setRequestHeader('Content-Type', 'application/json')

            // Optionally overwrite headers
            if (headers != null) {
                Object.keys(headers).forEach((key) => {
                    request.setRequestHeader(key, headers[key])
                })
            }

            // Initialize the request so a percentage loader can be displayed
            const requestId = GraphQL.requestCounter++
            GraphQL.requestPercentages[requestId] = 0

            // Send the load percentage to the subject
            GraphQL.sendLoadPercentageToSubject()

            // Whenever the request progresses
            request.onprogress = (event: ProgressEvent) => {

                // Update the percentage
                GraphQL.requestPercentages[requestId] = (event.loaded / event.total)

                // Send the load percentage to the subject
                GraphQL.sendLoadPercentageToSubject()
            }

            // Whenever the request is done
            request.onload = () => {

                // Remove the load percentage from the array
                delete GraphQL.requestPercentages[requestId]

                // Send the load percentage to the subject
                GraphQL.sendLoadPercentageToSubject()

                const headersArray = request.getAllResponseHeaders()
                    .split(/[\r\n]+/)
                    .filter((v) => v.length)

                const headers: any = {}
                for (const headersArrayPart of headersArray) {
                    const headersArrayParts = headersArrayPart.split(':').map((v) => v.trim())
                    headers[headersArrayParts[0].toLowerCase()] = headersArrayParts[1]
                }

                if (request.status < 400) {

                    let response: any
                    try {
                        response = JSON.parse(request.response)
                    } catch (e) {
                        response = request.response
                    }

                    resolve({
                        data: response.data,
                        status: request.status,
                        headers
                    })
                } else {
                    if (request.status === 401) {
                        GraphQL.unauthorized.next()
                    } else if (request.status === 500) {
                        GraphQL.internalServerError.next()
                    }

                    let response: any
                    try {
                        response = JSON.parse(request.response)
                    } catch (e) {
                        response = request.response
                    }

                    // eslint-disable-next-line
                    reject({
                        data: response,
                        status: request.status,
                        headers
                    })
                }
            }

            // When the request errors out
            request.onerror = () => {
                GraphQL.apiUnreachable.next()

                reject(request.statusText)
            }

            // Send the request
            request.send(JSON.stringify(payload))
        })
    }

    /**
     * Generate the headers for a http request
     */
    public static generateHeaders = (): any => {
        return {}
    }

    /**
     * Get the current load percentage for the pending requests
     */
    public static getCurrentLoadPercentage = () => {
        const values = Object.keys(GraphQL.requestPercentages)
            .map((i) => GraphQL.requestPercentages[i as any])
            .filter((p) => p < 1)

        if (values.length === 0) {
            return 100
        }

        return values.reduce((a, b) => a + b, 0) / values.length * 100
    }

    /**
     * Sends the load percentage to the subject
     */
    private static sendLoadPercentageToSubject = () => {
        GraphQL.requestProgressPercentage.next(
            GraphQL.getCurrentLoadPercentage()
        )
    }
}
