/**
 * The HTTP responses from the API.
 * 
 * @author Stan Hurks
 */
export default interface HttpResponse<T> {

    /**
     * The data
     */
    data: T

    /**
     * The response headers
     */
    headers: { [propertyName: string]: string }

    /**
     * The HTTP response code
     */
    status: number
}