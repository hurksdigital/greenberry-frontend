import React from 'react'
import Person from 'greenberry-common/models/entity/Person'
import image from './starwars-character.png'
import './starwars-character.scss'

/**
 * The props
 */
interface StarwarsCharacterProps {

    /**
     * The character
     */
    character: Person
}

/**
 * A character in star wars.
 * 
 * @author Stan Hurks
 */
export const StarwarsCharacter: React.FC<StarwarsCharacterProps> = (props) => {
    return (
        <div className="starwars-character">
            <div className="starwars-character-content">
                <div className="starwars-character-content-picture">
                    {/* TODO: Pictures are not available in SWAPI... although everything else works, and more ! */}
                    <img src={image} alt="Starwars character" />
                </div><div className="starwars-character-content-details">
                    <div className="starwars-character-content-details-name">
                        {
                            props.character.name
                        }
                    </div><div className="starwars-character-content-details-movie">
                        {
                            props.character.films.map((film) =>
                                <p key={film.uuid}>
                                    {
                                        film.title
                                    }
                                </p>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}