import React from 'react'
import FormSelect from './FormSelect'
import ReactDOM from 'react-dom'

describe('Form select component', () => {
    it('should render without crashing', () => {
        const div = document.createElement('div')

        const select = (
            <FormSelect
                value={"test"}
                placeholder="testt"
                options={[
                    {
                        key: 'test',
                        value: 'test'
                    }
                ]}
                onChange={(value) => { }} />
        )

        ReactDOM.render(select, div)
    })
})