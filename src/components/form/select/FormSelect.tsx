import React from 'react'
import './form-select.scss'

/**
 * The props
 */
interface FormSelectProps<T extends number | string> {

    /**
     * The selected key
     */
    value: T | null

    /**
     * The placeholder when no option is selected
     */
    placeholder: string

    /**
     * Key value pairs that represent the options in the select
     */
    options: Array<{
        /**
         * The key
         */
        key: T

        /**
         * The value
         */
        value: string
    }>

    /**
     * Whenever the value in the select changes
     */
    onChange: (value: T | null) => void
}

/**
 * A select in a form.
 * 
 * @author Stan Hurks
 */
export default class FormSelect extends React.Component<FormSelectProps<string>> {

    /**
     * The ref to the native select
     */
    private selectRef: React.RefObject<HTMLSelectElement> = React.createRef()

    public render = () => {
        return (
            <select className="form-select" onChange={(event) => {
                this.props.onChange(event.target.value === 'null' ? null : event.target.value)
            }} ref={this.selectRef} value={this.props.value || 'null'}>
                <option value="null">
                    {
                        this.props.placeholder
                    }
                </option>
                {
                    this.props.options
                        // Sort all entries alphabetically
                        .sort((x, y) => {
                            if (x.value > y.value) {
                                return 1
                            }
                            if (y.value > x.value) {
                                return -1
                            }
                            return 0
                        })
                        .map((option) =>
                            <option value={option.key} key={option.key}>
                                {
                                    option.value
                                }
                            </option>
                        )
                }
            </select>
        )
    }
}